#ifndef DIALOG_COMCONFIG_H
#define DIALOG_COMCONFIG_H

#include <QDialog>
#include <QComboBox>
#include <QPushButton>
#include "serial.h"

namespace Ui {
class Dialog_ComConfig;
}

class Dialog_ComConfig : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_ComConfig(QWidget *parent = nullptr);
    ~Dialog_ComConfig();

    Serial *mySerial = new Serial;

    Serial * Get_mySerial_ptr();

    QComboBox * Get_Com_comboBoxCom_Ptr();
    QComboBox * Get_Com_comboBoxBaud_Ptr();
    QComboBox * Get_Com_comboBoxCheck_Ptr();
    QComboBox * Get_Com_comboBoxData_Ptr();
    QComboBox * Get_Com_comboBoxStop_Ptr();
    QPushButton *Get_Com_Connect();

    void Set_Com_Flush_PushButton(bool state);



private slots:
    void on_pushButton_Cancel_clicked();

    void on_pushButton_Connect_clicked();

    void on_pushButton_ComFlush_clicked();

    void slot_disconnect2();



private:
    Ui::Dialog_ComConfig *ui;

signals:
    void signal_com_connect();
    void signal_com_disconnect();
};

#endif // DIALOG_COMCONFIG_H
