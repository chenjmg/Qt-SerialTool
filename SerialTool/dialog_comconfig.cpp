#include "dialog_comconfig.h"
#include "ui_dialog_comconfig.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QMessageBox>
#include "serial.h"

Dialog_ComConfig::Dialog_ComConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_ComConfig)
{
    ui->setupUi(this);
    setWindowTitle("串口配置");

    QPalette palette(this->palette());
    palette.setColor(QPalette::Background,QColor::fromRgb(245,245,245));
    this->setPalette(palette);
}

Dialog_ComConfig::~Dialog_ComConfig()
{
    delete ui;
}

void Dialog_ComConfig::on_pushButton_Cancel_clicked()
{
    this->close();
}

void Dialog_ComConfig::on_pushButton_Connect_clicked()
{
    if(mySerial->Get_Serial_Connect() == true)
    {
        QMessageBox::warning(NULL,"抱歉","串口已开启！"/*,QMessageBox::Yes*/);        

    }
    else if(mySerial->Get_Serial_Connect() == false)
    {
        QString portName = ui->comboBox_Com->currentText();
        qDebug()<<"portName: "<<portName<<endl;

        mySerial->Set_Serial_Com(portName);

        if(ui->comboBox_Baud->currentText() == tr("1200"))
        {
            mySerial->Set_Serial_Baud(QSerialPort::Baud1200);
        }
        else if(ui->comboBox_Baud->currentText() == tr("2400"))
        {
            mySerial->Set_Serial_Baud(QSerialPort::Baud2400);
        }
        else if(ui->comboBox_Baud->currentText() == tr("4800"))
        {
            mySerial->Set_Serial_Baud(QSerialPort::Baud4800);
        }
        else if(ui->comboBox_Baud->currentText() == tr("9600"))
        {
            mySerial->Set_Serial_Baud(QSerialPort::Baud9600);
        }
        else if(ui->comboBox_Baud->currentText() == tr("19200"))
        {
            mySerial->Set_Serial_Baud(QSerialPort::Baud19200);
        }
        else if(ui->comboBox_Baud->currentText() == tr("115200"))
        {
            mySerial->Set_Serial_Baud(QSerialPort::Baud115200);
        }

        if(ui->comboBox_Check->currentText() == tr("无"))
        {
            mySerial->Set_Serial_Check(QSerialPort::NoParity);
        }
        else if(ui->comboBox_Check->currentText() == tr("奇"))
        {
            mySerial->Set_Serial_Check(QSerialPort::OddParity);
        }
        else if(ui->comboBox_Check->currentText() == tr("偶"))
        {
            mySerial->Set_Serial_Check(QSerialPort::EvenParity);
        }

        if(ui->comboBox_Data->currentText() == tr("8"))
        {
            mySerial->Set_Serial_Data(QSerialPort::Data8);
        }
        else if(ui->comboBox_Data->currentText() == tr("7"))
        {
            mySerial->Set_Serial_Data(QSerialPort::Data7);
        }
        else if(ui->comboBox_Data->currentText() == tr("6"))
        {
            mySerial->Set_Serial_Data(QSerialPort::Data6);
        }
        else if(ui->comboBox_Data->currentText() == tr("5"))
        {
            mySerial->Set_Serial_Data(QSerialPort::Data5);
        }

        if(ui->comboBox_Stop->currentText() == tr("1"))
        {
            mySerial->Set_Serial_Stop(QSerialPort::OneStop);
        }
        else if(ui->comboBox_Stop->currentText() == tr("1.5"))
        {
            mySerial->Set_Serial_Stop(QSerialPort::OneAndHalfStop);
        }
        else if(ui->comboBox_Stop->currentText() == tr("2"))
        {
            mySerial->Set_Serial_Stop(QSerialPort::TwoStop);
        }

        mySerial->Open_Serial();

        if(mySerial->Get_Serial_Connect() == true)
        {
            ui->label_ComState->setText("串口 "+portName+" 已开启");

            emit signal_com_connect();

            ui->comboBox_Com->setEnabled(false);
            ui->comboBox_Baud->setEnabled(false);
            ui->comboBox_Data->setEnabled(false);
            ui->comboBox_Stop->setEnabled(false);
            ui->comboBox_Check->setEnabled(false);
            ui->pushButton_ComFlush->setText("关闭串口");
            ui->pushButton_Connect->setEnabled(false);

            this->close();
        }
        else {
            QMessageBox::warning(NULL,"抱歉","串口打开失败！"/*,QMessageBox::Yes*/);
        }
    }
}

void Dialog_ComConfig::on_pushButton_ComFlush_clicked()
{

//    if(mySerial->Get_Serial_Connect() == true)
//    {
//        ui->pushButton_ComFlush->setText("关闭串口");
//    }
//    else if(mySerial->Get_Serial_Connect() == false)
//    {
//        ui->pushButton_ComFlush->setText("刷新串口");
//    }

    if(mySerial->Get_Serial_Connect() == false)
    {
        ui->comboBox_Com->clear();
        foreach(const QSerialPortInfo &portinfo, QSerialPortInfo::availablePorts())
        {
            ui->comboBox_Com->addItem(portinfo.portName());
        }
    }
    else
    {
        mySerial->Close_Serial();
        ui->pushButton_ComFlush->setText("刷新串口");

        ui->comboBox_Com->setEnabled(true);
        ui->comboBox_Baud->setEnabled(true);
        ui->comboBox_Data->setEnabled(true);
        ui->comboBox_Stop->setEnabled(true);
        ui->comboBox_Check->setEnabled(true);
        ui->pushButton_Connect->setEnabled(true);

        emit signal_com_disconnect();
    }

}


void Dialog_ComConfig::slot_disconnect2()
{
    mySerial->Close_Serial();

    ui->comboBox_Com->setEnabled(true);
    ui->comboBox_Baud->setEnabled(true);
    ui->comboBox_Data->setEnabled(true);
    ui->comboBox_Stop->setEnabled(true);
    ui->comboBox_Check->setEnabled(true);
    ui->pushButton_Connect->setEnabled(true);
    ui->label_ComState->setText("串口已关闭");
}

Serial *Dialog_ComConfig::Get_mySerial_ptr()
{
    return mySerial;
}

QComboBox * Dialog_ComConfig::Get_Com_comboBoxCom_Ptr()
{
    return ui->comboBox_Com;
}
QComboBox * Dialog_ComConfig::Get_Com_comboBoxBaud_Ptr()
{
    return ui->comboBox_Baud;
}
QComboBox * Dialog_ComConfig::Get_Com_comboBoxCheck_Ptr()
{
    return ui->comboBox_Check;
}
QComboBox * Dialog_ComConfig::Get_Com_comboBoxData_Ptr()
{
    return ui->comboBox_Data;
}
QComboBox * Dialog_ComConfig::Get_Com_comboBoxStop_Ptr()
{
    return ui->comboBox_Stop;
}

QPushButton *Dialog_ComConfig::Get_Com_Connect()
{
    return ui->pushButton_Connect;
}

void Dialog_ComConfig::Set_Com_Flush_PushButton(bool state)
{
    if(state == true)
    {
        ui->pushButton_ComFlush->setText("关闭串口");
    }
    else {
        ui->pushButton_ComFlush->setText("刷新串口");
    }
}
