#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dialog_about.h"
#include "dialog_comconfig.h"
#include "dialog_console_1.h"
#include "dialog_console_2.h"
#include "dialog_console_3.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QByteArray serial_rec_data;
    int serial_rec_len;
    int serial_send_len;

    QByteArray HexStringToByteArray(QString HexString);
    QString    ByteArrayToHexString(QByteArray data, int len);

    void String_Convert_To_Hex(QString str, QByteArray &arr);

    char ConvertHexChar(char ch);

private slots:
    void on_config_triggered();

    void on_about_triggered();

    void on_quit_triggered();

    void slot_connect();

    void slot_disconnect();

    void on_disconnect_triggered();

    void on_connect_triggered();

    void slot_Read_Data();

    void on_pushButton_ClearCnt_clicked();

    void on_pushButton_ClearRxWin_clicked();

    void on_pushButton_Send_clicked();

    void on_pushButton_ClearTxWin_clicked();

    void on_pushButton_Con_Edit_1_clicked();

    void on_pushButton_Con_Edit_2_clicked();

    void on_pushButton_Con_Edit_3_clicked();

    void on_checkBox_2_stateChanged(int arg1);

    void on_checkBox_3_stateChanged(int arg1);

    void on_checkBox_4_stateChanged(int arg1);

    void slot_con1_enter();

    void slot_con2_enter();

    void slot_con3_enter();

    void on_pushButton_Send1_clicked();

    void on_pushButton_OpenFile_clicked();

private:
    Ui::MainWindow *ui;

    Dialog_About       *p_About;
    Dialog_ComConfig   *p_Com;
    Dialog_Console_1   *p_Con1;
    Dialog_Console_2   *p_Con2;
    Dialog_Console_3   *p_Con3;
signals:
    void signal_com_connect2();
    void signal_com_disconnect2();
};

#endif // MAINWINDOW_H
