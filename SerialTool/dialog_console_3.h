#ifndef DIALOG_CONSOLE_3_H
#define DIALOG_CONSOLE_3_H

#include <QDialog>
#include <QTextEdit>
#include <QComboBox>

namespace Ui {
class Dialog_Console_3;
}

class Dialog_Console_3 : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_Console_3(QWidget *parent = nullptr);
    ~Dialog_Console_3();

    QString Get_Label_Name();

    QTextEdit *Get_Con3_Edit_Prt(void);
    QComboBox *Get_ComboBox_Prt(void);

private:
    Ui::Dialog_Console_3 *ui;

    QString label_name;

private slots:

    void on_pushButton_Con3_Cancel_clicked();

    void on_pushButton_Con3_Enter_clicked();

signals:
    void signal_con1_edit();
};

#endif // DIALOG_CONSOLE_3_H
