#ifndef DIALOG_CONSOLE_2_H
#define DIALOG_CONSOLE_2_H

#include <QDialog>
#include <QTextEdit>
#include <QComboBox>

namespace Ui {
class Dialog_Console_2;
}

class Dialog_Console_2 : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_Console_2(QWidget *parent = nullptr);
    ~Dialog_Console_2();

    QString Get_Label_Name();

    QTextEdit *Get_Con2_Edit_Prt(void);
    QComboBox *Get_ComboBox_Prt(void);

private:
    Ui::Dialog_Console_2 *ui;

    QString label_name;

private slots:

    void on_pushButton_Con2_Enter_clicked();

    void on_pushButton_Con2_Cancel_clicked();

signals:
    void signal_con1_edit();
};

#endif // DIALOG_CONSOLE_2_H
