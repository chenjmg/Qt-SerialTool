#include "dialog_console_3.h"
#include "ui_dialog_console_3.h"

#include <QDebug>


Dialog_Console_3::Dialog_Console_3(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_Console_3)
{
    ui->setupUi(this);

    setWindowTitle("控制台自定义窗口");
}

Dialog_Console_3::~Dialog_Console_3()
{
    delete ui;
}



QString Dialog_Console_3::Get_Label_Name()
{
    return label_name;
}

void Dialog_Console_3::on_pushButton_Con3_Cancel_clicked()
{
    this->close();
}

void Dialog_Console_3::on_pushButton_Con3_Enter_clicked()
{
    label_name = ui->textEdit_Con3_Label->toPlainText();

    qDebug()<<label_name<<endl;

    emit signal_con1_edit();

    this->close();
}

QTextEdit *Dialog_Console_3::Get_Con3_Edit_Prt(void)
{
    return ui->textEdit_Con_Edit_3;
}

QComboBox *Dialog_Console_3::Get_ComboBox_Prt(void)
{
    return ui->comboBox_Con3;
}
