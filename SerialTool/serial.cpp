#include "serial.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QDebug>
#include <QObject>

#include "dialog_about.h"
#include "dialog_comconfig.h"
#include "mainwindow.h"

Serial::Serial()
{
    serial->close();
    disp_buff = 0;
    serial_open_flag = false;

//    connect(serial,SIGNAL(readyRead()),this,SLOT(slot_Read_Data()));

}

void Serial::Open_Serial()
{
    if(serial_open_flag == true)
    {
        QMessageBox::warning(NULL,"抱歉","串口已开启！"/*,QMessageBox::Yes*/);
        return;
    }

    serial->setPortName(Get_Serial_Com());
    serial->setParity(Get_Serial_Check());
    serial->setBaudRate(Get_Serial_Baud());
    serial->setDataBits(Get_Serial_Data());
    serial->setStopBits(Get_Serial_Stop());

    if(serial->open(QIODevice::ReadWrite) == false)
    {
        //QMessageBox::information(this,"抱歉","串口打开失败");
        QMessageBox::warning(NULL,"抱歉","串口打开失败！"/*,QMessageBox::Yes*/);

        qDebug()<<"串口打开失败"<<endl;
        serial_open_flag = false;
    }
    else
    {
        qDebug()<<"串口打开"<<endl;

        serial_open_flag = true;

    }
}

void Serial::Close_Serial()
{
    serial_open_flag = false;
    serial->close();
}

QString Serial::Get_Serial_Com()
{
    return com_num;
}

int Serial::Get_Serial_Baud()
{
    return baud_ratio;
}

QSerialPort::Parity Serial::Get_Serial_Check()
{
    return check_bit;
}

QSerialPort::DataBits Serial::Get_Serial_Data()
{
    return data_bit;
}

QSerialPort::StopBits Serial::Get_Serial_Stop()
{
    return stop_bit;
}

void Serial::Set_Serial_Com(const QString &com)
{
    com_num = com;

}

void Serial::Set_Serial_Baud(int baud)
{
    baud_ratio = baud;
}

void Serial::Set_Serial_Check(QSerialPort::Parity  check)
{
    check_bit = check;
}

void Serial::Set_Serial_Data(QSerialPort::DataBits data)
{
    data_bit = data;
}

void Serial::Set_Serial_Stop(QSerialPort::StopBits stop)
{
    stop_bit = stop;
}

bool Serial::Get_Serial_Connect()
{
    return serial_open_flag;
}
