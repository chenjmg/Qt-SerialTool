#ifndef SERIAL_H
#define SERIAL_H

#include <QSerialPort>
#include <QSerialPortInfo>

class Serial
{
public:
    Serial();

    QSerialPort *serial = new QSerialPort;

    char disp_buff;


    void Open_Serial();
    void Close_Serial();

    QString Get_Serial_Com();
    int     Get_Serial_Baud();
    QSerialPort::Parity   Get_Serial_Check();
    QSerialPort::DataBits Get_Serial_Data();
    QSerialPort::StopBits Get_Serial_Stop();

    void Set_Serial_Com(const QString &com);
    void Set_Serial_Baud(int baud);
    void Set_Serial_Check(QSerialPort::Parity  check);
    void Set_Serial_Data(QSerialPort::DataBits data);
    void Set_Serial_Stop(QSerialPort::StopBits stop);

    bool Get_Serial_Connect();

private:
    QString             com_num;
    int                 baud_ratio;
    QSerialPort::Parity   check_bit;
    QSerialPort::DataBits data_bit;
    QSerialPort::StopBits stop_bit;
    bool serial_open_flag;
};

#endif // SERIAL_H
