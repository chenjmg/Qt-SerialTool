#ifndef DIALOG_CONSOLE_1_H
#define DIALOG_CONSOLE_1_H

#include <QDialog>
#include <QTextEdit>
#include <QComboBox>

namespace Ui {
class Dialog_Console_1;
}

class Dialog_Console_1 : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_Console_1(QWidget *parent = nullptr);
    ~Dialog_Console_1();

    QString Get_Label_Name();

    QTextEdit *Get_Con1_Edit_Prt(void);
    QComboBox *Get_ComboBox_Prt(void);

private slots:

    void on_pushButton_Con1_Cancel_clicked();

    void on_pushButton_Con1_Enter_clicked();

private:
    Ui::Dialog_Console_1 *ui;

    QString label_name;

    QString Edit_buf;

signals:
    void signal_con1_edit();
};

#endif // DIALOG_CONSOLE_1_H
