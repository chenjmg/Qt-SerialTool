#-------------------------------------------------
#
# Project created by QtCreator 2019-08-02T23:59:57
#
#-------------------------------------------------

QT       += core gui serialport  winextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SerialTool
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        dialog_about.cpp \
        dialog_comconfig.cpp \
        dialog_console_1.cpp \
        dialog_console_2.cpp \
        dialog_console_3.cpp \
        dialog_sendfile.cpp \
        main.cpp \
        mainwindow.cpp \
        serial.cpp

HEADERS += \
        dialog_about.h \
        dialog_comconfig.h \
        dialog_console_1.h \
        dialog_console_2.h \
        dialog_console_3.h \
        dialog_sendfile.h \
        mainwindow.h \
        serial.h

FORMS += \
        dialog_about.ui \
        dialog_comconfig.ui \
        dialog_console_1.ui \
        dialog_console_2.ui \
        dialog_console_3.ui \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RC_ICONS += logo.ico

RESOURCES += \
    images.qrc
