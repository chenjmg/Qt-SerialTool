#ifndef DIALOG_SENDFILE_H
#define DIALOG_SENDFILE_H

#include <QString>

class dialog_SendFile
{
public:
    dialog_SendFile();

    void    do_file_Open();
    bool    do_file_Load(const QString & fileName);
};













#endif // DIALOG_SENDFILE_H
