#include "dialog_about.h"
#include "ui_dialog_about.h"

Dialog_About::Dialog_About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_About)
{
    ui->setupUi(this);

    setWindowTitle("关于");

    QPalette palette(this->palette());
    palette.setColor(QPalette::Background, Qt:: white);
    this->setPalette(palette);
}

Dialog_About::~Dialog_About()
{
    delete ui;
}

void Dialog_About::on_pushButton_AboutConfirm_clicked()
{
    this->close();
}
