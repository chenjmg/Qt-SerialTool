#include "dialog_console_1.h"
#include "ui_dialog_console_1.h"

#include <QDebug>

Dialog_Console_1::Dialog_Console_1(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_Console_1)
{
    ui->setupUi(this);

    setWindowTitle("控制台自定义窗口");
}

Dialog_Console_1::~Dialog_Console_1()
{
    delete ui;
}

void Dialog_Console_1::on_pushButton_Con1_Cancel_clicked()
{
    QString disp_buf;

    disp_buf = ui->textEdit_Con1_Edit->toPlainText();

    ui->textEdit_Con1_Edit->setText(disp_buf);
    this->close();
}

void Dialog_Console_1::on_pushButton_Con1_Enter_clicked()
{

    label_name = ui->textEdit_Con1_Label->toPlainText();

    qDebug()<<label_name<<endl;

    emit signal_con1_edit();
}

QString Dialog_Console_1::Get_Label_Name()
{
    return label_name;
}

QTextEdit *Dialog_Console_1::Get_Con1_Edit_Prt(void)
{
    return ui->textEdit_Con1_Edit;
}

QComboBox *Dialog_Console_1::Get_ComboBox_Prt(void)
{
    return ui->comboBox_Con1;
}
