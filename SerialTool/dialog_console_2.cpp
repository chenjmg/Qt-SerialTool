#include "dialog_console_2.h"
#include "ui_dialog_console_2.h"

#include <QDebug>

Dialog_Console_2::Dialog_Console_2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_Console_2)
{
    ui->setupUi(this);

    setWindowTitle("控制台自定义窗口");
}

Dialog_Console_2::~Dialog_Console_2()
{
    delete ui;
}

QString Dialog_Console_2::Get_Label_Name()
{
    return label_name;
}


void Dialog_Console_2::on_pushButton_Con2_Enter_clicked()
{
    label_name = ui->textEdit_Con2_Label->toPlainText();

    qDebug()<<label_name<<endl;

    emit signal_con1_edit();

    this->close();
}

void Dialog_Console_2::on_pushButton_Con2_Cancel_clicked()
{
    this->close();
}

QTextEdit *Dialog_Console_2::Get_Con2_Edit_Prt(void)
{
    return ui->textEdit_Con2_Eidt;
}

QComboBox *Dialog_Console_2::Get_ComboBox_Prt(void)
{
    return ui->comboBox_Con2;
}
