/********************************************************************************
** Form generated from reading UI file 'dialog_about.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_ABOUT_H
#define UI_DIALOG_ABOUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog_About
{
public:
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton_AboutConfirm;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;

    void setupUi(QDialog *Dialog_About)
    {
        if (Dialog_About->objectName().isEmpty())
            Dialog_About->setObjectName(QString::fromUtf8("Dialog_About"));
        Dialog_About->resize(639, 563);
        label = new QLabel(Dialog_About);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(230, 340, 211, 51));
        QFont font;
        font.setPointSize(20);
        label->setFont(font);
        label_2 = new QLabel(Dialog_About);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(270, 400, 91, 31));
        QFont font1;
        font1.setPointSize(15);
        label_2->setFont(font1);
        pushButton_AboutConfirm = new QPushButton(Dialog_About);
        pushButton_AboutConfirm->setObjectName(QString::fromUtf8("pushButton_AboutConfirm"));
        pushButton_AboutConfirm->setGeometry(QRect(260, 500, 101, 31));
        label_3 = new QLabel(Dialog_About);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(30, 60, 241, 241));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/QRCode1.jpg")));
        label_3->setScaledContents(true);
        label_4 = new QLabel(Dialog_About);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(250, 440, 141, 41));
        label_4->setFont(font1);
        label_5 = new QLabel(Dialog_About);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(340, 60, 251, 241));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/images/QRCode2.png")));
        label_5->setScaledContents(true);
        label_6 = new QLabel(Dialog_About);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(190, 320, 261, 16));

        retranslateUi(Dialog_About);

        QMetaObject::connectSlotsByName(Dialog_About);
    } // setupUi

    void retranslateUi(QDialog *Dialog_About)
    {
        Dialog_About->setWindowTitle(QCoreApplication::translate("Dialog_About", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("Dialog_About", "\344\270\262\345\217\243\350\260\203\350\257\225\345\267\245\345\205\267", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog_About", "By Jimmy", nullptr));
        pushButton_AboutConfirm->setText(QCoreApplication::translate("Dialog_About", "\347\241\256\345\256\232", nullptr));
        label_3->setText(QString());
        label_4->setText(QCoreApplication::translate("Dialog_About", "Version 1.0.2", nullptr));
        label_5->setText(QString());
        label_6->setText(QCoreApplication::translate("Dialog_About", "\346\211\253\347\240\201\345\205\263\346\263\250\344\275\234\350\200\205\357\274\214\346\210\226\350\200\205\357\274\214\346\215\220\351\222\261\347\273\231\344\275\234\350\200\205\346\244\215\345\217\221", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog_About: public Ui_Dialog_About {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_ABOUT_H
