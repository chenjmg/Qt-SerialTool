/********************************************************************************
** Form generated from reading UI file 'dialog_console_2.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_CONSOLE_2_H
#define UI_DIALOG_CONSOLE_2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Dialog_Console_2
{
public:
    QGroupBox *groupBox;
    QTextEdit *textEdit_Con2_Eidt;
    QPushButton *pushButton_Con2_Enter;
    QPushButton *pushButton_Con2_Cancel;
    QLabel *label;
    QLabel *label_2;
    QTextEdit *textEdit_Con2_Label;
    QComboBox *comboBox_Con2;

    void setupUi(QDialog *Dialog_Console_2)
    {
        if (Dialog_Console_2->objectName().isEmpty())
            Dialog_Console_2->setObjectName(QString::fromUtf8("Dialog_Console_2"));
        Dialog_Console_2->resize(473, 345);
        groupBox = new QGroupBox(Dialog_Console_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 451, 321));
        textEdit_Con2_Eidt = new QTextEdit(groupBox);
        textEdit_Con2_Eidt->setObjectName(QString::fromUtf8("textEdit_Con2_Eidt"));
        textEdit_Con2_Eidt->setGeometry(QRect(10, 60, 431, 101));
        pushButton_Con2_Enter = new QPushButton(groupBox);
        pushButton_Con2_Enter->setObjectName(QString::fromUtf8("pushButton_Con2_Enter"));
        pushButton_Con2_Enter->setGeometry(QRect(250, 270, 93, 28));
        pushButton_Con2_Cancel = new QPushButton(groupBox);
        pushButton_Con2_Cancel->setObjectName(QString::fromUtf8("pushButton_Con2_Cancel"));
        pushButton_Con2_Cancel->setGeometry(QRect(350, 270, 93, 28));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 40, 72, 15));
        QFont font;
        font.setItalic(true);
        label->setFont(font);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 170, 72, 15));
        label_2->setFont(font);
        textEdit_Con2_Label = new QTextEdit(groupBox);
        textEdit_Con2_Label->setObjectName(QString::fromUtf8("textEdit_Con2_Label"));
        textEdit_Con2_Label->setGeometry(QRect(10, 190, 431, 61));
        comboBox_Con2 = new QComboBox(groupBox);
        comboBox_Con2->addItem(QString());
        comboBox_Con2->addItem(QString());
        comboBox_Con2->setObjectName(QString::fromUtf8("comboBox_Con2"));
        comboBox_Con2->setGeometry(QRect(10, 272, 87, 26));

        retranslateUi(Dialog_Console_2);

        QMetaObject::connectSlotsByName(Dialog_Console_2);
    } // setupUi

    void retranslateUi(QDialog *Dialog_Console_2)
    {
        Dialog_Console_2->setWindowTitle(QCoreApplication::translate("Dialog_Console_2", "Dialog", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Dialog_Console_2", "\346\216\247\345\210\266\345\217\260\347\274\226\350\276\221", nullptr));
        pushButton_Con2_Enter->setText(QCoreApplication::translate("Dialog_Console_2", "\347\241\256\345\256\232", nullptr));
        pushButton_Con2_Cancel->setText(QCoreApplication::translate("Dialog_Console_2", "\345\217\226\346\266\210", nullptr));
        label->setText(QCoreApplication::translate("Dialog_Console_2", "\345\217\221\351\200\201\345\206\205\345\256\271", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog_Console_2", "\346\240\207\347\255\276", nullptr));
        comboBox_Con2->setItemText(0, QCoreApplication::translate("Dialog_Console_2", "Hex\345\217\221\351\200\201", nullptr));
        comboBox_Con2->setItemText(1, QCoreApplication::translate("Dialog_Console_2", "Char\345\217\221\351\200\201", nullptr));

    } // retranslateUi

};

namespace Ui {
    class Dialog_Console_2: public Ui_Dialog_Console_2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_CONSOLE_2_H
