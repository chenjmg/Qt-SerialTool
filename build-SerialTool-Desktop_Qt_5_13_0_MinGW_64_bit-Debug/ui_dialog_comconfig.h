/********************************************************************************
** Form generated from reading UI file 'dialog_comconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_COMCONFIG_H
#define UI_DIALOG_COMCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog_ComConfig
{
public:
    QGroupBox *groupBox;
    QComboBox *comboBox_Check;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_3;
    QComboBox *comboBox_Baud;
    QPushButton *pushButton_ComFlush;
    QLabel *label_2;
    QComboBox *comboBox_Stop;
    QPushButton *pushButton_Cancel;
    QComboBox *comboBox_Data;
    QPushButton *pushButton_Connect;
    QLabel *label_5;
    QComboBox *comboBox_Com;
    QLabel *label_ComState;

    void setupUi(QDialog *Dialog_ComConfig)
    {
        if (Dialog_ComConfig->objectName().isEmpty())
            Dialog_ComConfig->setObjectName(QString::fromUtf8("Dialog_ComConfig"));
        Dialog_ComConfig->resize(345, 366);
        groupBox = new QGroupBox(Dialog_ComConfig);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 321, 271));
        comboBox_Check = new QComboBox(Dialog_ComConfig);
        comboBox_Check->addItem(QString());
        comboBox_Check->addItem(QString());
        comboBox_Check->addItem(QString());
        comboBox_Check->setObjectName(QString::fromUtf8("comboBox_Check"));
        comboBox_Check->setGeometry(QRect(100, 160, 191, 22));
        label = new QLabel(Dialog_ComConfig);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(50, 80, 61, 20));
        label_4 = new QLabel(Dialog_ComConfig);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(50, 200, 61, 20));
        label_3 = new QLabel(Dialog_ComConfig);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(50, 160, 61, 20));
        comboBox_Baud = new QComboBox(Dialog_ComConfig);
        comboBox_Baud->addItem(QString());
        comboBox_Baud->addItem(QString());
        comboBox_Baud->addItem(QString());
        comboBox_Baud->addItem(QString());
        comboBox_Baud->addItem(QString());
        comboBox_Baud->addItem(QString());
        comboBox_Baud->addItem(QString());
        comboBox_Baud->setObjectName(QString::fromUtf8("comboBox_Baud"));
        comboBox_Baud->setGeometry(QRect(100, 120, 191, 22));
        pushButton_ComFlush = new QPushButton(Dialog_ComConfig);
        pushButton_ComFlush->setObjectName(QString::fromUtf8("pushButton_ComFlush"));
        pushButton_ComFlush->setGeometry(QRect(50, 41, 244, 25));
        label_2 = new QLabel(Dialog_ComConfig);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(50, 120, 61, 20));
        comboBox_Stop = new QComboBox(Dialog_ComConfig);
        comboBox_Stop->addItem(QString());
        comboBox_Stop->addItem(QString());
        comboBox_Stop->addItem(QString());
        comboBox_Stop->setObjectName(QString::fromUtf8("comboBox_Stop"));
        comboBox_Stop->setGeometry(QRect(100, 240, 191, 22));
        pushButton_Cancel = new QPushButton(Dialog_ComConfig);
        pushButton_Cancel->setObjectName(QString::fromUtf8("pushButton_Cancel"));
        pushButton_Cancel->setGeometry(QRect(180, 300, 111, 22));
        comboBox_Data = new QComboBox(Dialog_ComConfig);
        comboBox_Data->addItem(QString());
        comboBox_Data->addItem(QString());
        comboBox_Data->setObjectName(QString::fromUtf8("comboBox_Data"));
        comboBox_Data->setGeometry(QRect(100, 200, 191, 22));
        pushButton_Connect = new QPushButton(Dialog_ComConfig);
        pushButton_Connect->setObjectName(QString::fromUtf8("pushButton_Connect"));
        pushButton_Connect->setGeometry(QRect(50, 300, 111, 22));
        label_5 = new QLabel(Dialog_ComConfig);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(50, 240, 61, 20));
        comboBox_Com = new QComboBox(Dialog_ComConfig);
        comboBox_Com->setObjectName(QString::fromUtf8("comboBox_Com"));
        comboBox_Com->setGeometry(QRect(100, 80, 191, 22));
        label_ComState = new QLabel(Dialog_ComConfig);
        label_ComState->setObjectName(QString::fromUtf8("label_ComState"));
        label_ComState->setGeometry(QRect(50, 330, 121, 16));

        retranslateUi(Dialog_ComConfig);

        QMetaObject::connectSlotsByName(Dialog_ComConfig);
    } // setupUi

    void retranslateUi(QDialog *Dialog_ComConfig)
    {
        Dialog_ComConfig->setWindowTitle(QCoreApplication::translate("Dialog_ComConfig", "Dialog", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Dialog_ComConfig", "\344\270\262\345\217\243\351\205\215\347\275\256", nullptr));
        comboBox_Check->setItemText(0, QCoreApplication::translate("Dialog_ComConfig", "\346\227\240", nullptr));
        comboBox_Check->setItemText(1, QCoreApplication::translate("Dialog_ComConfig", "\345\245\207", nullptr));
        comboBox_Check->setItemText(2, QCoreApplication::translate("Dialog_ComConfig", "\345\201\266", nullptr));

        label->setText(QCoreApplication::translate("Dialog_ComConfig", "\344\270\262\345\217\243\345\217\267", nullptr));
        label_4->setText(QCoreApplication::translate("Dialog_ComConfig", "\346\225\260\346\215\256\344\275\215", nullptr));
        label_3->setText(QCoreApplication::translate("Dialog_ComConfig", "\346\240\241\351\252\214\344\275\215", nullptr));
        comboBox_Baud->setItemText(0, QCoreApplication::translate("Dialog_ComConfig", "115200", nullptr));
        comboBox_Baud->setItemText(1, QCoreApplication::translate("Dialog_ComConfig", "1200", nullptr));
        comboBox_Baud->setItemText(2, QCoreApplication::translate("Dialog_ComConfig", "2400", nullptr));
        comboBox_Baud->setItemText(3, QCoreApplication::translate("Dialog_ComConfig", "4800", nullptr));
        comboBox_Baud->setItemText(4, QCoreApplication::translate("Dialog_ComConfig", "9600", nullptr));
        comboBox_Baud->setItemText(5, QCoreApplication::translate("Dialog_ComConfig", "14400", nullptr));
        comboBox_Baud->setItemText(6, QCoreApplication::translate("Dialog_ComConfig", "25600", nullptr));

        pushButton_ComFlush->setText(QCoreApplication::translate("Dialog_ComConfig", "\345\210\267\346\226\260\344\270\262\345\217\243", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog_ComConfig", "\346\263\242\347\211\271\347\216\207", nullptr));
        comboBox_Stop->setItemText(0, QCoreApplication::translate("Dialog_ComConfig", "1", nullptr));
        comboBox_Stop->setItemText(1, QCoreApplication::translate("Dialog_ComConfig", "1.5", nullptr));
        comboBox_Stop->setItemText(2, QCoreApplication::translate("Dialog_ComConfig", "2", nullptr));

        pushButton_Cancel->setText(QCoreApplication::translate("Dialog_ComConfig", "\345\217\226\346\266\210", nullptr));
        comboBox_Data->setItemText(0, QCoreApplication::translate("Dialog_ComConfig", "8", nullptr));
        comboBox_Data->setItemText(1, QCoreApplication::translate("Dialog_ComConfig", "7", nullptr));

        pushButton_Connect->setText(QCoreApplication::translate("Dialog_ComConfig", "\350\277\236\346\216\245", nullptr));
        label_5->setText(QCoreApplication::translate("Dialog_ComConfig", "\345\201\234\346\255\242\344\275\215", nullptr));
        label_ComState->setText(QCoreApplication::translate("Dialog_ComConfig", "\344\270\262\345\217\243\346\234\252\350\277\236\346\216\245", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog_ComConfig: public Ui_Dialog_ComConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_COMCONFIG_H
