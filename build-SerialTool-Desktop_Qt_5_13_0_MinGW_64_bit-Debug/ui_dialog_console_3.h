/********************************************************************************
** Form generated from reading UI file 'dialog_console_3.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_CONSOLE_3_H
#define UI_DIALOG_CONSOLE_3_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Dialog_Console_3
{
public:
    QGroupBox *groupBox;
    QTextEdit *textEdit_Con_Edit_3;
    QPushButton *pushButton_Con3_Enter;
    QPushButton *pushButton_Con3_Cancel;
    QLabel *label;
    QLabel *label_2;
    QTextEdit *textEdit_Con3_Label;
    QComboBox *comboBox_Con3;

    void setupUi(QDialog *Dialog_Console_3)
    {
        if (Dialog_Console_3->objectName().isEmpty())
            Dialog_Console_3->setObjectName(QString::fromUtf8("Dialog_Console_3"));
        Dialog_Console_3->resize(476, 344);
        groupBox = new QGroupBox(Dialog_Console_3);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 451, 321));
        textEdit_Con_Edit_3 = new QTextEdit(groupBox);
        textEdit_Con_Edit_3->setObjectName(QString::fromUtf8("textEdit_Con_Edit_3"));
        textEdit_Con_Edit_3->setGeometry(QRect(10, 60, 431, 101));
        pushButton_Con3_Enter = new QPushButton(groupBox);
        pushButton_Con3_Enter->setObjectName(QString::fromUtf8("pushButton_Con3_Enter"));
        pushButton_Con3_Enter->setGeometry(QRect(250, 270, 93, 28));
        pushButton_Con3_Cancel = new QPushButton(groupBox);
        pushButton_Con3_Cancel->setObjectName(QString::fromUtf8("pushButton_Con3_Cancel"));
        pushButton_Con3_Cancel->setGeometry(QRect(350, 270, 93, 28));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 40, 72, 15));
        QFont font;
        font.setItalic(true);
        label->setFont(font);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 170, 72, 15));
        label_2->setFont(font);
        textEdit_Con3_Label = new QTextEdit(groupBox);
        textEdit_Con3_Label->setObjectName(QString::fromUtf8("textEdit_Con3_Label"));
        textEdit_Con3_Label->setGeometry(QRect(10, 190, 431, 61));
        comboBox_Con3 = new QComboBox(groupBox);
        comboBox_Con3->addItem(QString());
        comboBox_Con3->addItem(QString());
        comboBox_Con3->setObjectName(QString::fromUtf8("comboBox_Con3"));
        comboBox_Con3->setGeometry(QRect(10, 272, 87, 26));

        retranslateUi(Dialog_Console_3);

        QMetaObject::connectSlotsByName(Dialog_Console_3);
    } // setupUi

    void retranslateUi(QDialog *Dialog_Console_3)
    {
        Dialog_Console_3->setWindowTitle(QCoreApplication::translate("Dialog_Console_3", "Dialog", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Dialog_Console_3", "\346\216\247\345\210\266\345\217\260\347\274\226\350\276\221", nullptr));
        pushButton_Con3_Enter->setText(QCoreApplication::translate("Dialog_Console_3", "\347\241\256\345\256\232", nullptr));
        pushButton_Con3_Cancel->setText(QCoreApplication::translate("Dialog_Console_3", "\345\217\226\346\266\210", nullptr));
        label->setText(QCoreApplication::translate("Dialog_Console_3", "\345\217\221\351\200\201\345\206\205\345\256\271", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog_Console_3", "\346\240\207\347\255\276", nullptr));
        comboBox_Con3->setItemText(0, QCoreApplication::translate("Dialog_Console_3", "Hex\345\217\221\351\200\201", nullptr));
        comboBox_Con3->setItemText(1, QCoreApplication::translate("Dialog_Console_3", "Char\345\217\221\351\200\201", nullptr));

    } // retranslateUi

};

namespace Ui {
    class Dialog_Console_3: public Ui_Dialog_Console_3 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_CONSOLE_3_H
