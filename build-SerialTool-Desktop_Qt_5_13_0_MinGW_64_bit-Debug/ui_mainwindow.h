/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *save;
    QAction *quit;
    QAction *connect;
    QAction *disconnect;
    QAction *config;
    QAction *about;
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QTextBrowser *textBrowser_RecData;
    QLabel *label_ComStatus;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGroupBox *groupBox_2;
    QTextEdit *textEdit_SendData;
    QPushButton *pushButton_Send;
    QPushButton *pushButton_ClearCnt;
    QPushButton *pushButton_ClearRxWin;
    QPushButton *pushButton_ClearTxWin;
    QCheckBox *checkBox_EnterSend;
    QCheckBox *checkBox;
    QSpinBox *spinBox;
    QLabel *label_3;
    QComboBox *comboBox_disp;
    QComboBox *comboBox_2;
    QWidget *tab_2;
    QPushButton *pushButton_Con_Edit_1;
    QPushButton *pushButton_Con_Edit_2;
    QPushButton *pushButton_Con_Edit_3;
    QPushButton *pushButton_Send1;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QWidget *tab_3;
    QTextBrowser *textBrowser_FileName;
    QPushButton *pushButton_OpenFile;
    QComboBox *comboBox;
    QSpinBox *spinBox_2;
    QLabel *label_5;
    QLabel *label_8;
    QSpinBox *spinBox_3;
    QPushButton *pushButton;
    QProgressBar *progressBar;
    QLabel *label_9;
    QLabel *label_10;
    QWidget *tab_4;
    QLabel *label_6;
    QWidget *tab_5;
    QLabel *label_7;
    QGroupBox *groupBox_3;
    QGroupBox *groupBox_4;
    QLabel *label;
    QLabel *label_RX_Cnt;
    QGroupBox *groupBox_5;
    QLabel *label_2;
    QLabel *label_TX_Cnt;
    QGroupBox *groupBox_6;
    QLabel *label_4;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *menu_2;
    QMenu *menu_3;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(852, 625);
        save = new QAction(MainWindow);
        save->setObjectName(QString::fromUtf8("save"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        save->setIcon(icon);
        quit = new QAction(MainWindow);
        quit->setObjectName(QString::fromUtf8("quit"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/quit.png"), QSize(), QIcon::Normal, QIcon::Off);
        quit->setIcon(icon1);
        connect = new QAction(MainWindow);
        connect->setObjectName(QString::fromUtf8("connect"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/connect.png"), QSize(), QIcon::Normal, QIcon::Off);
        connect->setIcon(icon2);
        disconnect = new QAction(MainWindow);
        disconnect->setObjectName(QString::fromUtf8("disconnect"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/disconnect.png"), QSize(), QIcon::Normal, QIcon::Off);
        disconnect->setIcon(icon3);
        config = new QAction(MainWindow);
        config->setObjectName(QString::fromUtf8("config"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/set1.png"), QSize(), QIcon::Normal, QIcon::Off);
        config->setIcon(icon4);
        about = new QAction(MainWindow);
        about->setObjectName(QString::fromUtf8("about"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/about.png"), QSize(), QIcon::Normal, QIcon::Off);
        about->setIcon(icon5);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 831, 301));
        textBrowser_RecData = new QTextBrowser(groupBox);
        textBrowser_RecData->setObjectName(QString::fromUtf8("textBrowser_RecData"));
        textBrowser_RecData->setGeometry(QRect(10, 20, 811, 271));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(textBrowser_RecData->sizePolicy().hasHeightForWidth());
        textBrowser_RecData->setSizePolicy(sizePolicy);
        textBrowser_RecData->setAutoFillBackground(false);
        textBrowser_RecData->setStyleSheet(QString::fromUtf8(""));
        label_ComStatus = new QLabel(centralWidget);
        label_ComStatus->setObjectName(QString::fromUtf8("label_ComStatus"));
        label_ComStatus->setGeometry(QRect(20, 530, 101, 16));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(10, 320, 831, 201));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        tabWidget->setMaximumSize(QSize(16777000, 16777000));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 10, 801, 161));
        textEdit_SendData = new QTextEdit(groupBox_2);
        textEdit_SendData->setObjectName(QString::fromUtf8("textEdit_SendData"));
        textEdit_SendData->setGeometry(QRect(10, 20, 681, 91));
        pushButton_Send = new QPushButton(groupBox_2);
        pushButton_Send->setObjectName(QString::fromUtf8("pushButton_Send"));
        pushButton_Send->setGeometry(QRect(700, 20, 91, 91));
        pushButton_Send->setLayoutDirection(Qt::LeftToRight);
        pushButton_Send->setAutoFillBackground(true);
        pushButton_Send->setStyleSheet(QString::fromUtf8(""));
        pushButton_ClearCnt = new QPushButton(groupBox_2);
        pushButton_ClearCnt->setObjectName(QString::fromUtf8("pushButton_ClearCnt"));
        pushButton_ClearCnt->setGeometry(QRect(10, 128, 75, 23));
        pushButton_ClearRxWin = new QPushButton(groupBox_2);
        pushButton_ClearRxWin->setObjectName(QString::fromUtf8("pushButton_ClearRxWin"));
        pushButton_ClearRxWin->setGeometry(QRect(100, 128, 75, 23));
        pushButton_ClearTxWin = new QPushButton(groupBox_2);
        pushButton_ClearTxWin->setObjectName(QString::fromUtf8("pushButton_ClearTxWin"));
        pushButton_ClearTxWin->setGeometry(QRect(190, 128, 75, 23));
        checkBox_EnterSend = new QCheckBox(groupBox_2);
        checkBox_EnterSend->setObjectName(QString::fromUtf8("checkBox_EnterSend"));
        checkBox_EnterSend->setGeometry(QRect(280, 132, 101, 16));
        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(390, 132, 91, 16));
        spinBox = new QSpinBox(groupBox_2);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setGeometry(QRect(490, 130, 71, 22));
        spinBox->setMaximum(10000);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(570, 135, 54, 12));
        comboBox_disp = new QComboBox(groupBox_2);
        comboBox_disp->addItem(QString());
        comboBox_disp->addItem(QString());
        comboBox_disp->setObjectName(QString::fromUtf8("comboBox_disp"));
        comboBox_disp->setGeometry(QRect(610, 130, 80, 22));
        comboBox_2 = new QComboBox(groupBox_2);
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        comboBox_2->setGeometry(QRect(710, 130, 80, 22));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        pushButton_Con_Edit_1 = new QPushButton(tab_2);
        pushButton_Con_Edit_1->setObjectName(QString::fromUtf8("pushButton_Con_Edit_1"));
        pushButton_Con_Edit_1->setGeometry(QRect(80, 20, 221, 28));
        pushButton_Con_Edit_2 = new QPushButton(tab_2);
        pushButton_Con_Edit_2->setObjectName(QString::fromUtf8("pushButton_Con_Edit_2"));
        pushButton_Con_Edit_2->setGeometry(QRect(80, 70, 221, 28));
        pushButton_Con_Edit_3 = new QPushButton(tab_2);
        pushButton_Con_Edit_3->setObjectName(QString::fromUtf8("pushButton_Con_Edit_3"));
        pushButton_Con_Edit_3->setGeometry(QRect(80, 120, 221, 28));
        pushButton_Send1 = new QPushButton(tab_2);
        pushButton_Send1->setObjectName(QString::fromUtf8("pushButton_Send1"));
        pushButton_Send1->setGeometry(QRect(680, 110, 131, 51));
        checkBox_2 = new QCheckBox(tab_2);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setGeometry(QRect(50, 25, 91, 19));
        checkBox_3 = new QCheckBox(tab_2);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(50, 75, 91, 19));
        checkBox_4 = new QCheckBox(tab_2);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));
        checkBox_4->setGeometry(QRect(50, 125, 91, 19));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        textBrowser_FileName = new QTextBrowser(tab_3);
        textBrowser_FileName->setObjectName(QString::fromUtf8("textBrowser_FileName"));
        textBrowser_FileName->setGeometry(QRect(10, 12, 641, 30));
        pushButton_OpenFile = new QPushButton(tab_3);
        pushButton_OpenFile->setObjectName(QString::fromUtf8("pushButton_OpenFile"));
        pushButton_OpenFile->setGeometry(QRect(670, 9, 141, 71));
        comboBox = new QComboBox(tab_3);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(10, 60, 100, 28));
        spinBox_2 = new QSpinBox(tab_3);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        spinBox_2->setGeometry(QRect(195, 60, 60, 28));
        spinBox_2->setMinimum(1);
        spinBox_2->setMaximum(10000);
        label_5 = new QLabel(tab_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(130, 65, 61, 16));
        QFont font;
        font.setKerning(true);
        label_5->setFont(font);
        label_8 = new QLabel(tab_3);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(280, 65, 71, 16));
        spinBox_3 = new QSpinBox(tab_3);
        spinBox_3->setObjectName(QString::fromUtf8("spinBox_3"));
        spinBox_3->setGeometry(QRect(355, 60, 60, 28));
        pushButton = new QPushButton(tab_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(670, 90, 141, 71));
        progressBar = new QProgressBar(tab_3);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(10, 100, 651, 31));
        progressBar->setValue(90);
        progressBar->setOrientation(Qt::Horizontal);
        label_9 = new QLabel(tab_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(10, 145, 81, 16));
        label_10 = new QLabel(tab_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(110, 146, 54, 12));
        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        label_6 = new QLabel(tab_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(270, 20, 170, 130));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/images/\347\254\254\344\270\211\346\226\271\346\226\275\345\267\245.png")));
        label_6->setScaledContents(true);
        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        label_7 = new QLabel(tab_5);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(270, 20, 170, 130));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/images/\347\254\254\344\270\211\346\226\271\346\226\275\345\267\245.png")));
        label_7->setScaledContents(true);
        tabWidget->addTab(tab_5, QString());
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 528, 120, 20));
        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(140, 527, 120, 21));
        label = new QLabel(groupBox_4);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 0, 21, 20));
        label_RX_Cnt = new QLabel(groupBox_4);
        label_RX_Cnt->setObjectName(QString::fromUtf8("label_RX_Cnt"));
        label_RX_Cnt->setGeometry(QRect(40, 2, 71, 16));
        groupBox_5 = new QGroupBox(centralWidget);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(270, 528, 120, 20));
        label_2 = new QLabel(groupBox_5);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 0, 21, 20));
        label_TX_Cnt = new QLabel(groupBox_5);
        label_TX_Cnt->setObjectName(QString::fromUtf8("label_TX_Cnt"));
        label_TX_Cnt->setGeometry(QRect(40, 2, 71, 16));
        groupBox_6 = new QGroupBox(centralWidget);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(400, 528, 120, 20));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(640, 530, 181, 16));
        MainWindow->setCentralWidget(centralWidget);
        groupBox_3->raise();
        groupBox_4->raise();
        tabWidget->raise();
        groupBox->raise();
        label_ComStatus->raise();
        groupBox_5->raise();
        groupBox_6->raise();
        label_4->raise();
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 852, 26));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        menu_2 = new QMenu(menuBar);
        menu_2->setObjectName(QString::fromUtf8("menu_2"));
        menu_3 = new QMenu(menuBar);
        menu_3->setObjectName(QString::fromUtf8("menu_3"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menu->menuAction());
        menuBar->addAction(menu_2->menuAction());
        menuBar->addAction(menu_3->menuAction());
        menu->addAction(save);
        menu->addAction(quit);
        menu_2->addAction(connect);
        menu_2->addAction(disconnect);
        menu_2->addAction(config);
        menu_3->addAction(about);
        mainToolBar->addAction(connect);
        mainToolBar->addAction(disconnect);
        mainToolBar->addAction(config);
        mainToolBar->addSeparator();
        mainToolBar->addAction(about);
        mainToolBar->addSeparator();
        mainToolBar->addAction(quit);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        save->setText(QCoreApplication::translate("MainWindow", "\344\277\235\345\255\230(&S)", nullptr));
        quit->setText(QCoreApplication::translate("MainWindow", "\351\200\200\345\207\272(&Q)", nullptr));
        connect->setText(QCoreApplication::translate("MainWindow", "\350\277\236\346\216\245(&C)", nullptr));
        disconnect->setText(QCoreApplication::translate("MainWindow", "\346\226\255\345\274\200(&D)", nullptr));
        config->setText(QCoreApplication::translate("MainWindow", "\351\205\215\347\275\256(&S)", nullptr));
        about->setText(QCoreApplication::translate("MainWindow", "\345\205\263\344\272\216(&A)", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "\346\216\245\346\224\266\347\252\227\345\217\243", nullptr));
        label_ComStatus->setText(QCoreApplication::translate("MainWindow", "\344\270\262\345\217\243\345\267\262\345\205\263\351\227\255", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201\347\252\227\345\217\243", nullptr));
        pushButton_Send->setText(QCoreApplication::translate("MainWindow", " \345\217\221\351\200\201", nullptr));
        pushButton_ClearCnt->setText(QCoreApplication::translate("MainWindow", "\350\256\241\346\225\260\346\270\205\351\233\266", nullptr));
        pushButton_ClearRxWin->setText(QCoreApplication::translate("MainWindow", " \346\216\245\346\224\266\346\270\205\347\251\272", nullptr));
        pushButton_ClearTxWin->setText(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201\346\270\205\347\251\272", nullptr));
        checkBox_EnterSend->setText(QCoreApplication::translate("MainWindow", "\345\270\246\345\233\236\350\275\246\345\217\221\351\200\201", nullptr));
        checkBox->setText(QCoreApplication::translate("MainWindow", "\345\256\232\346\227\266\345\217\221\351\200\201", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "ms/\346\254\241", nullptr));
        comboBox_disp->setItemText(0, QCoreApplication::translate("MainWindow", "Hex\346\230\276\347\244\272", nullptr));
        comboBox_disp->setItemText(1, QCoreApplication::translate("MainWindow", "Char\346\230\276\347\244\272", nullptr));

        comboBox_2->setItemText(0, QCoreApplication::translate("MainWindow", "Hex\345\217\221\351\200\201", nullptr));
        comboBox_2->setItemText(1, QCoreApplication::translate("MainWindow", "Char\345\217\221\351\200\201", nullptr));

        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201\347\252\227", nullptr));
        pushButton_Con_Edit_1->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\256\232\344\271\211", nullptr));
        pushButton_Con_Edit_2->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\256\232\344\271\211", nullptr));
        pushButton_Con_Edit_3->setText(QCoreApplication::translate("MainWindow", "\350\207\252\345\256\232\344\271\211", nullptr));
        pushButton_Send1->setText(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201", nullptr));
        checkBox_2->setText(QString());
        checkBox_3->setText(QString());
        checkBox_4->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "\346\216\247\345\210\266\345\217\260", nullptr));
        pushButton_OpenFile->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\274\200\346\226\207\344\273\266", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("MainWindow", "PT/INR", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("MainWindow", "PT/PT", nullptr));
        comboBox->setItemText(2, QCoreApplication::translate("MainWindow", "APTT/QC", nullptr));
        comboBox->setItemText(3, QCoreApplication::translate("MainWindow", "PT/APTT", nullptr));
        comboBox->setItemText(4, QCoreApplication::translate("MainWindow", "RACE", nullptr));
        comboBox->setItemText(5, QCoreApplication::translate("MainWindow", "QC Ctrl", nullptr));

        label_5->setText(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201\346\235\241\346\225\260", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", " \351\227\264\351\232\224\346\227\266\351\227\264", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "\345\267\262\345\217\221\351\200\201\346\235\241\346\225\260", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("MainWindow", "\345\217\221\351\200\201\346\226\207\344\273\266", nullptr));
        label_6->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("MainWindow", " \344\270\212\344\274\240\346\226\207\344\273\266", nullptr));
        label_7->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QCoreApplication::translate("MainWindow", "  \345\233\272\344\273\266\345\215\207\347\272\247", nullptr));
        groupBox_3->setTitle(QString());
        groupBox_4->setTitle(QString());
        label->setText(QCoreApplication::translate("MainWindow", "Rx\357\274\232", nullptr));
        label_RX_Cnt->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        groupBox_5->setTitle(QString());
        label_2->setText(QCoreApplication::translate("MainWindow", "Tx\357\274\232", nullptr));
        label_TX_Cnt->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        groupBox_6->setTitle(QString());
        label_4->setText(QCoreApplication::translate("MainWindow", " \344\270\262\345\217\243\344\270\213\350\275\275\345\267\245\345\205\267 By Jimmy", nullptr));
        menu->setTitle(QCoreApplication::translate("MainWindow", "\346\226\207\344\273\266\357\274\210&F\357\274\211", nullptr));
        menu_2->setTitle(QCoreApplication::translate("MainWindow", "\344\270\262\345\217\243\350\256\276\347\275\256(&S)", nullptr));
        menu_3->setTitle(QCoreApplication::translate("MainWindow", "\345\270\256\345\212\251(&H)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
