/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../SerialTool/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[28];
    char stringdata0[617];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 19), // "signal_com_connect2"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 22), // "signal_com_disconnect2"
QT_MOC_LITERAL(4, 55, 19), // "on_config_triggered"
QT_MOC_LITERAL(5, 75, 18), // "on_about_triggered"
QT_MOC_LITERAL(6, 94, 17), // "on_quit_triggered"
QT_MOC_LITERAL(7, 112, 12), // "slot_connect"
QT_MOC_LITERAL(8, 125, 15), // "slot_disconnect"
QT_MOC_LITERAL(9, 141, 23), // "on_disconnect_triggered"
QT_MOC_LITERAL(10, 165, 20), // "on_connect_triggered"
QT_MOC_LITERAL(11, 186, 14), // "slot_Read_Data"
QT_MOC_LITERAL(12, 201, 30), // "on_pushButton_ClearCnt_clicked"
QT_MOC_LITERAL(13, 232, 32), // "on_pushButton_ClearRxWin_clicked"
QT_MOC_LITERAL(14, 265, 26), // "on_pushButton_Send_clicked"
QT_MOC_LITERAL(15, 292, 32), // "on_pushButton_ClearTxWin_clicked"
QT_MOC_LITERAL(16, 325, 32), // "on_pushButton_Con_Edit_1_clicked"
QT_MOC_LITERAL(17, 358, 32), // "on_pushButton_Con_Edit_2_clicked"
QT_MOC_LITERAL(18, 391, 32), // "on_pushButton_Con_Edit_3_clicked"
QT_MOC_LITERAL(19, 424, 26), // "on_checkBox_2_stateChanged"
QT_MOC_LITERAL(20, 451, 4), // "arg1"
QT_MOC_LITERAL(21, 456, 26), // "on_checkBox_3_stateChanged"
QT_MOC_LITERAL(22, 483, 26), // "on_checkBox_4_stateChanged"
QT_MOC_LITERAL(23, 510, 15), // "slot_con1_enter"
QT_MOC_LITERAL(24, 526, 15), // "slot_con2_enter"
QT_MOC_LITERAL(25, 542, 15), // "slot_con3_enter"
QT_MOC_LITERAL(26, 558, 27), // "on_pushButton_Send1_clicked"
QT_MOC_LITERAL(27, 586, 30) // "on_pushButton_OpenFile_clicked"

    },
    "MainWindow\0signal_com_connect2\0\0"
    "signal_com_disconnect2\0on_config_triggered\0"
    "on_about_triggered\0on_quit_triggered\0"
    "slot_connect\0slot_disconnect\0"
    "on_disconnect_triggered\0on_connect_triggered\0"
    "slot_Read_Data\0on_pushButton_ClearCnt_clicked\0"
    "on_pushButton_ClearRxWin_clicked\0"
    "on_pushButton_Send_clicked\0"
    "on_pushButton_ClearTxWin_clicked\0"
    "on_pushButton_Con_Edit_1_clicked\0"
    "on_pushButton_Con_Edit_2_clicked\0"
    "on_pushButton_Con_Edit_3_clicked\0"
    "on_checkBox_2_stateChanged\0arg1\0"
    "on_checkBox_3_stateChanged\0"
    "on_checkBox_4_stateChanged\0slot_con1_enter\0"
    "slot_con2_enter\0slot_con3_enter\0"
    "on_pushButton_Send1_clicked\0"
    "on_pushButton_OpenFile_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x06 /* Public */,
       3,    0,  140,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,  141,    2, 0x08 /* Private */,
       5,    0,  142,    2, 0x08 /* Private */,
       6,    0,  143,    2, 0x08 /* Private */,
       7,    0,  144,    2, 0x08 /* Private */,
       8,    0,  145,    2, 0x08 /* Private */,
       9,    0,  146,    2, 0x08 /* Private */,
      10,    0,  147,    2, 0x08 /* Private */,
      11,    0,  148,    2, 0x08 /* Private */,
      12,    0,  149,    2, 0x08 /* Private */,
      13,    0,  150,    2, 0x08 /* Private */,
      14,    0,  151,    2, 0x08 /* Private */,
      15,    0,  152,    2, 0x08 /* Private */,
      16,    0,  153,    2, 0x08 /* Private */,
      17,    0,  154,    2, 0x08 /* Private */,
      18,    0,  155,    2, 0x08 /* Private */,
      19,    1,  156,    2, 0x08 /* Private */,
      21,    1,  159,    2, 0x08 /* Private */,
      22,    1,  162,    2, 0x08 /* Private */,
      23,    0,  165,    2, 0x08 /* Private */,
      24,    0,  166,    2, 0x08 /* Private */,
      25,    0,  167,    2, 0x08 /* Private */,
      26,    0,  168,    2, 0x08 /* Private */,
      27,    0,  169,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->signal_com_connect2(); break;
        case 1: _t->signal_com_disconnect2(); break;
        case 2: _t->on_config_triggered(); break;
        case 3: _t->on_about_triggered(); break;
        case 4: _t->on_quit_triggered(); break;
        case 5: _t->slot_connect(); break;
        case 6: _t->slot_disconnect(); break;
        case 7: _t->on_disconnect_triggered(); break;
        case 8: _t->on_connect_triggered(); break;
        case 9: _t->slot_Read_Data(); break;
        case 10: _t->on_pushButton_ClearCnt_clicked(); break;
        case 11: _t->on_pushButton_ClearRxWin_clicked(); break;
        case 12: _t->on_pushButton_Send_clicked(); break;
        case 13: _t->on_pushButton_ClearTxWin_clicked(); break;
        case 14: _t->on_pushButton_Con_Edit_1_clicked(); break;
        case 15: _t->on_pushButton_Con_Edit_2_clicked(); break;
        case 16: _t->on_pushButton_Con_Edit_3_clicked(); break;
        case 17: _t->on_checkBox_2_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_checkBox_3_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->on_checkBox_4_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->slot_con1_enter(); break;
        case 21: _t->slot_con2_enter(); break;
        case 22: _t->slot_con3_enter(); break;
        case 23: _t->on_pushButton_Send1_clicked(); break;
        case 24: _t->on_pushButton_OpenFile_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::signal_com_connect2)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::signal_com_disconnect2)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::signal_com_connect2()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void MainWindow::signal_com_disconnect2()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
